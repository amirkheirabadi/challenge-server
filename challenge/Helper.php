<?php

namespace Challenge;

use Validator;

/**
 *
 */
class Helper
{
    public static function RandomString($length = 15)
    {
        $randomString = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyz!=?_$@#*&'),
                0, $length);

        return $randomString;
    }

    public static function VldFails($rules, $inputs)
    {
        $validator = Validator::make($inputs, $rules);

        return $validator->fails();
    }

    public static function Vld($rules, $inputs)
    {
        $messages = [];

        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            foreach ($validator->messages()->all() as $error) {
                array_push($messages, $error);
            }

            return response()->json(['code' => Errors::ValidationError, 'messages' => $messages], 500);
        }
    }
}
