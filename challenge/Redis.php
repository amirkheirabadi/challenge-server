<?php

namespace Challenge;

use Predis;

/**
 *
 */
class Redis
{
    public static $connection;

    public static function connect($datatable)
    {
        self::$connection = new Predis\Client('tcp://127.0.0.1?database='.$datatable.'&persistent=1');
    }

    public static function ExistsKey($key)
    {
        return self::$connection->EXISTS($key);
    }

    public static function SetToken($userId)
    {
        self::connect(env('REDIS_USER', 1));
        $randomString = Helper::RandomString(15);

        if (self::ExistsKey($randomString)) {
            return self::SetToken($userId);
        }

        self::$connection->set($randomString, $userId);
        self::$connection->expire($randomString, env('REDIS_USER_EXPIRE', 60 * 60));

        return $randomString;
    }
}
