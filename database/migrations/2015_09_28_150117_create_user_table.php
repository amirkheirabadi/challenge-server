<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users', function ($table) {
                $table->increments('user_id');
                $table->string('user_mobile');
                $table->string('user_password');
                $table->string('user_fname')->nullable();
                $table->string('user_lname')->nullable();
                $table->enum('user_status', ['pending', 'active', 'suspend'])->default('pending');

                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('users');
    }
}
