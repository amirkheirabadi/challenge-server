<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Challenge\Helper;
use Hash;
use App\Users;

/**
 *
 */
class AuthController extends Controller
{
    public function signin(Request $request)
    {
        $rules = [
            'mobile' => 'required|digits_between:12,12|numeric|unique:users,user_mobile',
            'password' => 'required|between:6,12|alpha_num',
        ];
        if (Helper::VldFails($rules, $request->all())) {
            return Helper::Vld($rules, $request->all());
        }

        return Users::signin($request->get('mobile'), $request->get('password'));
    }

    public function signup(Request $request)
    {
        $rules = [
            'mobile' => 'required|digits_between:12,12|numeric|unique:users,user_mobile',
            'password' => 'required|between:6,12|alpha_num',
        ];
        if (Helper::VldFails($rules, $request->all())) {
            return Helper::Vld($rules, $request->all());
        }
        $user = new Users();
        $user->user_mobile = $request->get('mobile');
        $user->user_password = Hash::make($request->get('password'));

        return $user->signup();
    }
}
