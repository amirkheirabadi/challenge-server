<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Challenge\Redis;

class Users extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'user_id';

    protected $hidden = ['user_password'];

    public function signup()
    {
        $this->user_status = 'pending';
        $this->save();
        $token = Redis::SetToken($this->user_id);

        return response()->json(['token' => $token], 200);
    }
}
